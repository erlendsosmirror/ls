/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <dirent.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <simpleprint/simpleprint.h>

///////////////////////////////////////////////////////////////////////////////
// Constants
///////////////////////////////////////////////////////////////////////////////
const char setcolorblue[] = {0x1B, 0x5B, '3', '4', ';', '1', 'm', 0};
const char setcoloryellow[] = {0x1B, 0x5B, '3', '3', ';', '1', 'm', 0};
const char setcolormagenta[] = {0x1B, 0x5B, '3', '5', ';', '1', 'm', 0};
const char resetcolor[] = {0x1B, 0x5B, '0', 'm', 0};

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void PrintEntry (char *name, char *visiblename, int args, int *xpos)
{
	// Ignore "." entries
	if (!strcmp (visiblename, "."))
		return;

	// Ignore ".." entries
	if (!strcmp (visiblename, ".."))
		return;

	// Stat the file in order to get type and size
	struct stat st;
	int ret = stat (name, &st);
	int mode = st.st_mode & S_IFMT;

	// Check result
	if (ret < 0)
	{
		PrintError ("ls", name);
		return;
	}

	// If we are doing ls -l
	if (args)
	{
		// Print mode
		char modestr[] = "-rwx ";

		if (mode == S_IFDIR)
			modestr[0] = 'd';
		else if (mode == S_IFCHR)
			modestr[0] = 'c';
		else if (mode == S_IFIFO)
			modestr[0] = 'p';

		SimplePrint (modestr);

		// Print size
		char str[16];
		_itoa (st.st_size, str);
		SimplePrint (str);
		SimplePrint (" ");
	}

	// Set the correct color
	if (mode == S_IFDIR)
		SimplePrint ((char*)setcolorblue);
	else if (mode == S_IFCHR)
		SimplePrint ((char*)setcoloryellow);
	else if (mode == S_IFIFO)
		SimplePrint ((char*)setcolormagenta);

	// Check if we need a new line
	if (!args)
	{
		*xpos = *xpos + strlen (visiblename) + 1;

		if (*xpos > 29)
		{
			SimplePrint ("\n");
			*xpos = 0;
		}
	}

	// Print the name
	SimplePrint (visiblename);

	// Reset the color
	if (mode == S_IFDIR ||
		mode == S_IFCHR ||
		mode == S_IFIFO)
		SimplePrint ((char*)resetcolor);

	// Print either newline or space according to mode
	if (args)
		SimplePrint ("\n");
	else
		SimplePrint (" ");
}

char *AddSlashIfNotExists (char *input)
{
	// Find the null pointer of input
	char *end = input;
	while (*end) end++;

	// If str is non-empty, step back (point to last char before null)
	if (end != input) end--;

	// Then add '/' if it is missing
	if (*end != '/')
	{
		end++;
		*end++ = '/';
		*end = 0;
	}
	else if (*end)
		end++;

	return end;
}

int LsHelper (char *path, int args)
{
	// Open directory
	DIR *d = opendir (path);

	// Check for success and print each directory
	if (d)
	{
		struct dirent *dir;
		int xpos = 0;

		while ((dir = readdir (d)) != 0)
		{
			// Copy path
			char str[64];
			strcpy (str, path);

			// Add forward slash if it is missing
			char *end = AddSlashIfNotExists (str);

			// Append name
			strcpy (end, dir->d_name);

			// Print the entry
			PrintEntry (str, dir->d_name, args, &xpos);
		}

		closedir (d);
	}
	else
		PrintError ("ls", path);

	return 1;
}

int ParseArgument (const char *str)
{
	int args = 0;

	while (*str)
	{
		if (*str == 'l')
			args |= 1;
		else
		{
			SimplePrint ("Unknown option\n");
			return 1;
		}

		str++;
	}

	return args;
}

int main (int argc, char **argv)
{
	// Usage
	if (argc < 1)
	{
		SimplePrint ("Usage: ls <args> <folder>\n");
		return 1;
	}

	// Args and folder parsed
	int args = 0;
	int folder = 0;

	// For each argv, parse if option and print if otherwise
	for (int i = 1; i < argc; i++)
	{
		if (argv[i][0] == '-')
			args |= ParseArgument (&argv[i][1]);
		else
			folder = LsHelper (argv[i], args);
	}

	// If we did not print any directories, print the curernt
	if (!folder)
		LsHelper (".", args);

	// Has to add a newline if -l was not specified
	if (!args)
		SimplePrint ("\n");

	return 0;
}
